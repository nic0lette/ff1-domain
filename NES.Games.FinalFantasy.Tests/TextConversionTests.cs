namespace NES.Games.FinalFantasy.Tests {
    using System;
    using Xunit;

    using NES.Games.FinalFantasy.Text;

    public class TextConversionTests {
        [Theory,
        InlineData(0, "{Separator}"),
        InlineData(212, "{Sword}"),
        InlineData(213, "{Hammer}"),
        InlineData(214, "{Dagger}"),
        InlineData(215, "{Axe}"),
        InlineData(216, "{Staff}"),
        InlineData(217, "{Nunchuck}"),
        InlineData(218, "{Armour}"),
        InlineData(219, "{Shield}"),
        InlineData(220, "{Helmet}"),
        InlineData(221, "{Gauntlet}"),
        InlineData(222, "{Bracelet}"),
        InlineData(223, "{Robe}"),
        InlineData(224, "{Percent}"),
        InlineData(225, "{Potion}"),
        InlineData(255, "{Space}")]
        public void TestGlyphEncoding(byte code, string name) {
            Assert.Equal(code, EncodeSingle(name));
        }

        // Given +str+, should return encode to a single byte.
        private byte EncodeSingle(string str) {
            var b = FFTextConverter.Encode(str);

            Assert.Single(b);
            return b[0];
        }
    }
}
