namespace NES.Games.FinalFantasy.Tests.Roms.Usa {
    using Xunit;

    using FinalFantasy.Monsters;

    public class MonsterTests {
        [Theory,
        InlineData(0, "IMP", 8, 6, 6, 16, Genus.Giant, OnHitEffect.None), // IMP
        InlineData(127, "CHAOS", 2000, 0, 0, 200, Genus.None, OnHitEffect.Stun) // CHAOS
        ]
        public void TestMonsterStats(int idx, string name, int hp, int xp, int gp, int mdef, Genus genus, OnHitEffect onhit) {
            var rom = Helper.USAFromEnv();
            var mob = rom.Monsters[idx];

            Assert.Equal(name, mob.Name);
            Assert.Equal(hp, mob.Stats.HP);
            Assert.Equal(xp, mob.Stats.XP);
            Assert.Equal(gp, mob.Stats.GP);
            Assert.Equal(mdef, mob.Stats.MagicDefence);
            Assert.Equal(genus, mob.Stats.Genus);
            Assert.Equal(onhit, mob.Stats.OnHitEffect);
        }
    }
}
