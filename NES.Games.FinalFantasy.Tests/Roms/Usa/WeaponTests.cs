namespace NES.Games.FinalFantasy.Tests.Roms.Usa {
    using Xunit;

    public class WeaponTests {
        [Theory,
        InlineData(0, "Wooden{Nunchuck}", 12, 10), // Wooden Nunchucks
        InlineData(37, "Katana{Space}", 33, 30), // Katana
        InlineData(39, "Masmune", 56, 10), // Masmune
        ]
        public void TestWeaponStats(int idx, string name, int dmg, int crit) {
            var rom = Helper.USAFromEnv();
            var wpn = rom.Weapons[idx];

            Assert.Equal(name, wpn.Name);
            Assert.Equal(dmg, wpn.Stats.Damage);
            Assert.Equal(crit, wpn.Stats.CriticalHitChance);
        }
    }
}
