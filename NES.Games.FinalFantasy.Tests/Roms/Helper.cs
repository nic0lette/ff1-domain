﻿namespace NES.Games.FinalFantasy.Tests.Roms {
    using System;
    using System.Collections.Generic;
    using System.Text;
    using NES.Games.FinalFantasy.Roms;

    static class Helper {
        public static FFUsa USAFromEnv() {
            var path = Environment.GetEnvironmentVariable("FF_NES_DEV_ROM");
            return new FFUsa(path);
        }
    }
}
