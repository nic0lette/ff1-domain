﻿# NAME

NES.Games.FinalFantasy.Tests - Tests for the `NES.Games.FinalFantasy` module.


# SYNOPSIS

```shell
# From this project's directory...
dotnet test

# ...or from the repository's root
dotnet test NES.Games.FinalFantasy.Tests
```


# REQUIREMENTS

- [.NET Core Runtime ~> 2.1](http://get.dot.net/)
- An environment variable for `FF_NES_DEV_ROM` pointing to a
  vanilla Final Fantasy (USA) ROM.


# DESCRIPTION

`NES.Games.FinalFantasy.Tests` is the test suite for the 
`NES.Games.FinalFantasy` module, intended for use by developers 
interacting with Final Fantasy ROM files. For example, 
[Final Fantasy Randomizer] developers.

You will need to set the `FF_NES_DEV_ROM` environment variable to
a readable Final Fantasy (USA) ROM's path. On Windows, this can be achieved
by running `setx FF_NES_DEV_ROM <PathToRom>` from the command line.

Note that due to how environment variables work on Windows, you will need to
restart Visual Studio if it was running when you ran the `setx` command before
the test runner can detect it.

On sane operating systems, the simplest way is to add an entry to
your shell's initialization (e.g. `.bashrc` or `.zshrc`) as
`export FF_NES_DEV_ROM="$HOME/totally-legit-roms/ff1-usa.nes"` and then
source it `source ~/.zshrc`.

If you don't want to set an environment variable for every session, you
could also run the tests with `FF_NES_DEV_ROM="path" dotnet test` every time.


# AUTHORS

- Brian Edmonds (Artea) `<brian@bedmonds.net>`, initial version.


# COPYRIGHT & LICENCE

Copyright 2018 Brian Edmonds

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



[Final Fantasy Randomizer]: https://github.com/FiendsOfTheElements/FF1Randomizer
