namespace NES.Games.FinalFantasy.Weapons {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using FinalFantasy.IO;
    using FinalFantasy.Text;

    class WeaponStatBlock : FixedWidthBlock<IWeaponStats> {
        public WeaponStatBlock(int address) : base(address, 8, 40) {}
        
        public WeaponStatBlock(int address, IRom rom)
            : base(address, 8, 40)
        {
            Load(rom);
        }

        protected override IWeaponStats Build(byte[] buf)
            => new WeaponStats(buf);
    }


    class WeaponPartition : NamedPartition<IWeaponStats, WeaponStatBlock> {
        public WeaponPartition(ITextBlock names, WeaponStatBlock stats)
            : base(names, stats) {}
    }
}
