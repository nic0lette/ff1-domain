﻿namespace NES.Games.FinalFantasy.Text {
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;

    using NES.Games.FinalFantasy.Text.Encoding;

    /// <summary>
    /// Conversion of text between Strings and FF1's internal 
    /// representation.
    /// </summary>
    public static class FFTextConverter {
        public static readonly IEnumerable<int> Alpha =
          Enumerable.Range(0x80, 62).ToArray();

        public static string Decode(byte[] data) {
            var str = new StringBuilder();

            foreach (var b in data) str.Append(ByteToTextMap[b]);

            return str.ToString();
        }

        // Strictly speaking, "chr" can also be a glyph.
        public static string Decode(byte chr) {
            return ByteToTextMap[chr];
        }

        /// <summary>
        /// Encode a string into a byte sequence as used by Final Fantasy,
        /// applying optional 
        /// </summary>
        /// <param name="text"></param>
        /// <param name="behaviour"></param>
        /// <exception cref="EndOfStreamException">
        ///	Raised when attempting to parse a glyph that is not closed.
        ///	For example: "{Sword"
        /// </exception>
        /// <exception cref="ArgumentException" />
        /// <returns></returns>
        public static byte[] Encode(
            string text,
            Behaviour behaviour = new Behaviour()
        ) {
            var buf = UtfToFFBytes(text, behaviour.UseDTETable);

            if (behaviour.MaxLength <= 0)
                return buf;

            if (buf.Length > behaviour.MaxLength)
                throw new ArgumentException("Encoded text is larger than allowed size");

            if (behaviour.Padding == Padding.None)
                return buf;

            int len = behaviour.MaxLength - buf.Length;
            byte[] pad = new byte[len];
            for (int i = 0; i < len; ++i)
                pad[i] = behaviour.PaddingChar;

            return behaviour.Padding == Padding.PadLeft
                ? pad.Concat(buf).ToArray()
                : buf.Concat(pad).ToArray();
        }


        static byte ReadGlyph(StringReader r) {
            var name = new StringBuilder();

            if ((char)r.Read() != '{')
                throw new ArgumentException("First character is not the start of a glyph sequence");

            while (r.Peek() != '}') {
                if (r.Peek() == -1)
                    throw new EndOfStreamException("Attempting to read a glyph, found end of string.");

                name.Append((char)r.Read());
            }

            r.Read();

            return (byte)Enum.Parse(typeof(Glyph), name.ToString());
        }

        static byte[] UtfToFFBytes(string str, bool useDTE) {
            var buf = new List<byte>();

            using (var r = new StringReader(str)) {
                while (r.Peek() != -1) {
                    if ((char)r.Peek() == '{') {
                        buf.Add(ReadGlyph(r));
                        continue;
                    }

                    char c = (char)r.Read();

                    if (useDTE && IsTwoCharByte(c, r.Peek())) {
                        buf.Add(EncodeTwoChar(c, (char)r.Read()));
                        continue;
                    }

                    buf.Add(Encode(c));
                }
            }

            return buf.ToArray();
        }


        static byte Encode(char c) {
            if (!TextToByteMap.ContainsKey(c.ToString()))
                throw new ArgumentException($"Unsupported character: {c}");

            return TextToByteMap[c.ToString()];
        }

        static bool IsTwoCharByte(char c, int peek) {
            if (peek == -1) return false;
            return TextToByteMap.ContainsKey(new String(c, (char)peek));
        }

        static byte EncodeTwoChar(char a, char b) {
            return TextToByteMap[new string(a, b)];
        }

        static readonly Dictionary<byte, string> ByteToTextMap;
        static Dictionary<string, byte> TextToByteMap => DTETable.Dictionary;


        static FFTextConverter() {
            ByteToTextMap = new Dictionary<byte, string>();

            LoadDTETable();
            LoadGlyphs();

            foreach (var pair in ByteToTextMap) {
                if (TextToByteMap.ContainsKey(pair.Value)) continue;
                TextToByteMap.Add(pair.Value, pair.Key);
            }
        }

        /// <summary>
        /// Map the contents of the DTE Table.
        /// </summary>
        static void LoadDTETable() {
            foreach (var ln in TextToByteMap) {
                if (ByteToTextMap.ContainsKey(ln.Value)) continue;
                ByteToTextMap.Add(ln.Value, ln.Key);
            }
        }

        /// <summary>
        /// Add glyphs to the conversion maps.
        /// </summary>
        /// <remarks>
        /// Glyphs are, for example, the sword or shield icons.
        /// 
        /// To use them in text, simply put their name (as per the TextGlyph enum)
        /// in brackets. For example {Sword} will insert a Sword glyph.
        /// </remarks>
        static void LoadGlyphs() {
            foreach (Glyph glyph in Enum.GetValues(typeof(Glyph))) {
                if (ByteToTextMap.ContainsKey((byte)glyph)) continue;
                ByteToTextMap.Add(
                  (byte)glyph,
                  $"{{{Enum.GetName(typeof(Glyph), glyph)}}}"
                );
            }
        }
    }
}
