﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NES.Games.FinalFantasy.Text {
    /// <summary>
    /// Represents a group of strings in the primary text data block.
    /// </summary>
    public enum FFStringGroup {
        // Items, Weapons, Armour, GoldValues, Spells, Class names
        StandardText,
        EnemySkills,
        EnemyNames,
        Dialogue,
        BattleMessages,
    }
}
