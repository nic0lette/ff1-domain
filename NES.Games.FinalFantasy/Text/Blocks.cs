﻿// Text is stored rather awkwardly in Final Fantasy.
// Most of it is grouped together in the following order:
//
// 1. NPC Dialogue
// 2. Enemy Skill Names
// 3. Player-related Text (Items, Weapons, Armour, Spell Names, 
//    Character Classes)
// 4. Battle messages ("Exiled to 4th dimension")
// 5. Enemy names.
//
// Each region starts off with a block of pointers, 
// followed by a block of data.
// There is plenty of "garbage" data left over, however.
namespace NES.Games.FinalFantasy.Text {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using FinalFantasy.IO;

    class TextData : VariableWidthBlock<FFString> {
        public TextData(
            int address,
            int maxAddress,
            int initialCount
        ) : base(
            address, 
            maxAddress, 
            initialCount,
            SeparationBehaviour.StandardText
        ) {}

        protected override FFString Build(byte[] buf)
            => new FFString(buf);
    }

    public interface ITextBlock {
        FFString this[int index] { get; set; }
        void Save(IRom rom);
        void Load(IRom rom);
        bool Loaded { get; }
    }

    class TextBlock : AddressedBlock<FFString, TextData>, ITextBlock {
        public TextBlock(
            int pointerOffset,
            int pointersStart,
            int dataStart,
            int dataEnd,
            int count
        ) : base(
            pointerOffset,
            pointersStart,
            new TextData(dataStart, dataEnd, count),
            count
        ) { }

        public TextBlock(
            int pointerOffset,
            int pointersStart,
            int dataStart,
            int dataEnd,
            int count,
            IRom rom
        ) : this(pointerOffset, pointersStart, dataStart, dataEnd, count) {
            Load(rom);
        }


        public ITextBlock SubSection(int offset, int count) {
            return new TextSubBlock(this, offset, count);
        }


        class TextSubBlock : ITextBlock {
            public TextSubBlock(TextBlock parent, int start, int count) {
                Parent = parent;
                Offset = start;
                End = start + count;
            }

            TextBlock Parent { get; }
            int Offset { get; }
            int End { get; }

            public FFString this[int index] {
                get {
                    if (index + Offset > End)
                        throw new ArgumentOutOfRangeException();

                    return Parent[index + Offset];
                }

                set {
                    if (index + Offset > End)
                        throw new ArgumentOutOfRangeException();
                    Parent[index + Offset] = value;
                }
            }

            public void Load(IRom rom) => Parent.Load(rom);
            public void Save(IRom rom) => Parent.Save(rom);
            public bool Loaded => Parent.Loaded;
        }
    }
}
