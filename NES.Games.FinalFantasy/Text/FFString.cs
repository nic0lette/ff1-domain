﻿namespace NES.Games.FinalFantasy.Text {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using FinalFantasy.Text.Encoding;

    /// <summary>
    /// Represents a string of text encoded using the DTE table.
    /// </summary>
    public class FFString : IRomValueType {
        byte[] raw;

        public FFString(byte[] raw) {
            this.raw = raw;
        }

        public string Value
        {
            get => FFTextConverter.Decode(raw);
            set => raw = FFTextConverter.Encode(value, Behaviour.None);
        }

        public int Size => raw.Length;
        public byte[] ToBytes() => raw;
    }
}
