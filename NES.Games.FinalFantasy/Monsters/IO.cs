﻿namespace NES.Games.FinalFantasy.Monsters {
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    using FinalFantasy.IO;
    using FinalFantasy.Monsters;
    using FinalFantasy.Text;

    class MonsterStatBlock
        : FixedWidthBlock<IMonsterStats>, IRomValueList<IMonsterStats>
    {
        public MonsterStatBlock(int address) : base(address, 20, 128) { }

        public MonsterStatBlock(int address, IRom rom)
            : base(address, 20, 128) {
            Load(rom);
        }

        protected override IMonsterStats Build(byte[] buf)
            => new MonsterStats(buf);
    }

    class MonsterPartition : NamedPartition<IMonsterStats, MonsterStatBlock> {
        TextBlock Names { get; }
        MonsterStatBlock Stats { get; }

        public MonsterPartition(TextBlock names, MonsterStatBlock stats)
            : base(names, stats) {}
    }
}
