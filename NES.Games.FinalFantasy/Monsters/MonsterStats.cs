﻿namespace NES.Games.FinalFantasy.Monsters {
    using System;
    using System.ComponentModel;
    using FinalFantasy.Converters;
    using FinalFantasy.IO;

    public interface IMonsterStats : IRomValueType {
        /// <summary>
        /// Experience points yield for defeating this monster.
        /// </summary>
        ushort XP { get; set; }

        /// <summary>
        /// Gold pieces yield for defeating this monster.
        /// </summary>
        ushort GP { get; set; }

        /// <summary>
        /// This monster's starting hit points.
        /// </summary>
        ushort HP { get; set; }

        /// <summary>
        /// This monster's morale.
        /// </summary>
        byte Morale { get; set; }

        /// <summary>
        /// This monster's AI script index.
        /// </summary>
        /// <remarks>
        /// 0x00 = MeleeOnly
        /// </remarks>
        byte AIScript { get; set; }

        /// <summary>
        /// This monster's ability to dodge melee attacks.
        /// </summary>
        byte Evasion { get; set; }

        /// <summary>
        /// This monster's resistance to physical damage.
        /// </summary>
        byte Absorb { get; set; }

        /// <summary>
        /// This monster's number of melee swings.
        /// </summary>
        byte AttackRolls { get; set; }

        /// <summary>
        /// The accuracy of each of this monster's melee swings.
        /// </summary>
        byte Accuracy { get; set; }

        /// <summary>
        /// Minimum damage for each of this monster's melee swings.
        /// </summary>
        /// <remarks>
        /// Damage range: [Damage..Damage * 2]
        /// If this is a critical hit, add another [Damage..Damage * 2] that
        /// will not be reduced by the defender's absorb.
        /// </remarks>
        byte Damage { get; set; }

        /// <summary>
        /// The chance, out of 200, that an individual melee swing
        /// will be a critical hit.
        /// </summary>
        byte CriticalHitChance { get; set; }

        /// <summary>
        /// How often this monster's on-hit effect is likely to trigger
        /// for each melee swing.
        /// </summary>
        OnHitFrequency OnHitFrequency { get; set; }

        /// <summary>
        /// The status effect that could be applied to the defender
        /// on each melee swing.
        /// </summary>
        OnHitEffect OnHitEffect { get; set; }

        /// <summary>
        /// This monster's family.
        /// </summary>
        /// <remarks>
        /// For example, Dragon or Giant.
        /// </remarks>
        Genus Genus { get; set; }

        /// <summary>
        /// How reistant to magic this monster is.
        /// </summary>
        byte MagicDefence { get; set; }

        /// <summary>
        /// The elements that this monster is weak to.
        /// </summary>
        /// <remarks>
        /// Spells using an element that the monster is weak to
        /// will have a higher chance to land, and deal increased
        /// damage.
        /// </remarks>
        Element Weaknesses { get; }

        /// <summary>
        /// The elements that this monster is resistant to.
        /// </summary>
        /// <remarks>
        /// Spells using an element that the monster is resistant to
        /// will have a signficantly reduced chance to land.
        /// </remarks>
        Element Resistances { get; }
    }

    class MonsterStats : FixedWidthRomValue, IMonsterStats {
        public MonsterStats(byte[] raw) : base(raw) {}

        public override int Size => 20;

        public ushort XP
        {
            get => LittleEndian.LoadFrom(raw, 0);
            set => LittleEndian.CopyTo(value, raw, 0);
        }

        public ushort GP
        {
            get => LittleEndian.LoadFrom(raw, 2);
            set => LittleEndian.CopyTo(value, raw, 2);
        }

        public ushort HP
        {
            get => LittleEndian.LoadFrom(raw, 4);
            set => LittleEndian.CopyTo(value, raw, 4);
        }

        public byte Morale
        {
            get => raw[6];
            set => raw[6] = value;
        }

        public byte AIScript
        {
            get => raw[7];
            set => raw[7] = value;
        }

        public byte Evasion
        {
            get => raw[8];
            set => raw[8] = value;
        }

        public byte Absorb
        {
            get => raw[9];
            set => raw[9] = value;
        }

        public byte AttackRolls
        {
            get => raw[10];
            set => raw[10] = value;
        }

        public byte Accuracy
        {
            get => raw[11];
            set => raw[11] = value;
        }

        public byte Damage
        {
            get => raw[12];
            set => raw[12] = value;
        }

        public byte CriticalHitChance
        {
            get => raw[13];
            set => raw[13] = value;
        }

        public OnHitFrequency OnHitFrequency
        {
            get => (OnHitFrequency)raw[14];
            set => raw[14] = (byte)value;
        }

        public OnHitEffect OnHitEffect
        {
            get => (OnHitEffect)raw[15];
            set => raw[15] = (byte)value;
        }

        public Genus Genus
        {
            get => (Genus)raw[16];
            set => raw[16] = (byte)value;
        }

        public byte MagicDefence
        {
            get => raw[17];
            set => raw[17] = value;
        }

        public Element Weaknesses
        {
            get => (Element)raw[18];
            set => raw[18] = (byte)value;
        }

        public Element Resistances
        {
            get => (Element)raw[19];
            set => raw[19] = (byte)value;
        }
    }
}
