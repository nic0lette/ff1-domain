namespace NES.Games.FinalFantasy {
    /// <summary>
    /// The item definition for a piece of armour.
    /// </summary>
    public interface IArmourStats : IRomValueType {
        /// <summary>
        /// Evasion penalty applied to the character
        /// equiping this armour.
        /// </summary>
        byte Weight { get; set; }

        /// <summary>
        /// Defense bonus granted by this armour.
        /// </summary>
        byte Absorb { get; set; }

        /// <summary>
        /// Elemental resistances granted by this armour.
        /// </summary>
        Element Resistances { get; set; }

        /// <summary>
        /// Index of the spell cast when using this armour
        /// through the Item combat menu.
        /// </summary>
        byte CastOnUse { get; set; }
    }
}
