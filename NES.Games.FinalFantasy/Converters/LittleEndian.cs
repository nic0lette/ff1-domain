﻿namespace NES.Games.FinalFantasy.Converters {
    using System.Diagnostics;

    /// <summary>
    /// Implementation of a BitConverter that converts
    /// to and from little-endian byte arrays.
    /// </summary>
    /// <remarks>
    /// All numbers in FF1 are stored in LittleEndian.
    /// </remarks>
    static class LittleEndian {
        /// <summary>
        /// Copies the bytes of value to buf, starting at index.
        /// </summary>
        /// <param name="value">The value to copy.</param>
        /// <param name="buf">The buffer in which to copy the bytes.</param>
        /// <param name="index">Starting index for the copy into the buffer.</param>
        public static void CopyTo(ushort value, byte[] buf, int index) {
            for (int i = 0; i < sizeof(ushort); ++i) {
                buf[i + index] = unchecked((byte)(value & 0xFF));
                value = (ushort)(value >> 8);
            }
        }

        public static ushort LoadFrom(byte[] buf, int index) {
            Debug.Assert(buf.Length >= sizeof(ushort) + index - 1);

            ushort ret = 0;

            for (int i = 0; i < sizeof(ushort); ++i) {
                ret = (ushort)unchecked((ret << 8) | buf[index + sizeof(ushort) - 1 - i]);
            }

            return ret;
        }

        /// <summary>
        /// Returns the representation of value as a
        /// little endian byte array.
        /// </summary>
        /// <param name="value">Value to convert.</param>
        /// <returns>value as a little endian byte[2]</returns>
        public static byte[] ToBytes(ushort value) {
            var buf = new byte[sizeof(ushort)];

            for (int i = 0; i < sizeof(ushort); ++i) {
                buf[i] = unchecked((byte)(value & 0xFF));
                value = (ushort)(value >> 8);
            }

            return buf;
        }

        /// <summary>
        /// Returns the numerical value of a little-endian byte[2].
        /// </summary>
        /// <param name="buf">Little endian byte[2] to convert.</param>
        /// <returns>The value represented by the two bytes.</returns>
        public static ushort ToUShort(byte[] buf) {
            Debug.Assert(buf.Length == sizeof(ushort));

            ushort ret = 0;

            for (int i = 0; i < sizeof(ushort); ++i) {
                ret = (ushort)unchecked((ret << 8) | buf[sizeof(ushort) - 1 - i]);
            }

            return ret;
        }
    }
}
