﻿namespace NES.Games.FinalFantasy.Roms {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using FinalFantasy.IO;

    /// <summary>
    /// Manages lazy-loading and updating of a ROM's different partitions.
    /// </summary>
    class PartitionTable {
        IRom Rom { get; }

        public PartitionTable(IRom rom) {
            Rom = rom;
            Named = new Dictionary<Type, object>();
            Unnamed = new Dictionary<Type, object>();
        }

        Dictionary<Type, object> Named { get; }
        Dictionary<Type, object> Unnamed { get; }
    
        public void RegisterNamed<T>(
            IRomSerializableNamedList<T> partition
        ) where T : IRomValueType {
            Named.Add(typeof(T), partition);
        }

        public void Register<T>(
            IRomSerializableList<T> partition
        ) where T : IRomValueType {
            Unnamed.Add(typeof(T), partition);
        }

        public IRomSerializableNamedList<T> GetNamed<T>()
            where T : IRomValueType
        {
            var item = Named[typeof(T)] as IRomSerializableNamedList<T>;

            if (item == null) throw new KeyNotFoundException($"{typeof(T)} is not a recognized partition of named items.");
            if (!item.Loaded) item.Load(Rom);

            return item;
        }

        public void UpdateNamed<T>(INamedList<T> value)
            where T : IRomValueType
        {
            var partition = GetNamed<T>();

            for (int i = 0; i < value.Count; ++i) {
                partition[i] = value[i];
            }

            partition.Save(Rom);
        }

               
        public IRomSerializableList<T> Get<T>()
            where T : IRomValueType
        {
            var item = Unnamed[typeof(T)] as IRomSerializableList<T>;

            if (item == null) throw new KeyNotFoundException($"{typeof(T)} is not a recognized partition of unnamed items.");
            if (!item.Loaded) item.Load(Rom);

            return item;
        }

        public void Update<T>(IRomValueList<T> value)
            where T : IRomValueType
        {
            var partition = Get<T>();

            for (int i = 0; i < value.Count; ++i) {
                partition[i] = value[i];
            }

            partition.Save(Rom);
        }
    }
}
