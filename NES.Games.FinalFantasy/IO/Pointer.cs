﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using NES.Games.FinalFantasy.Converters;

    class Pointer : IRomValueType {
        byte[] raw;

        public Pointer(byte[] raw) {
            if (raw.Length != Size) {
                throw new ArgumentException("Pointers must be exactly 2 bytes long.");
            }

            this.raw = raw;
        }

        public ushort PageAddress
        {
            get => LittleEndian.LoadFrom(raw, 0);
            set => LittleEndian.CopyTo(value, raw, 0);
        }

        public int Size => 2;
        public byte[] ToBytes() => raw;
    }
}
