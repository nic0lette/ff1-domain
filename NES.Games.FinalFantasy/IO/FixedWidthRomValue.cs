﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using System.ComponentModel;

    abstract class FixedWidthRomValue : IRomValueType {
        public abstract int Size { get; }

        protected byte[] raw;

        public FixedWidthRomValue(byte[] raw) {
            if (raw.Length != Size) {
                var klass = GetType().Name;
                throw new ArgumentException($"{klass} must be {Size} bytes long.");
            }

            this.raw = raw;
        }

        public byte[] ToBytes() => raw;
    }
}
