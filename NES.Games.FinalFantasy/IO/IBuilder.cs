﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NES.Games.FinalFantasy.IO {
    interface IBuilder<T> where T : IRomValueType {
        T Build();
    }
}
