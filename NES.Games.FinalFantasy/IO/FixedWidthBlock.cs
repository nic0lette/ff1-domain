﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Base class for a splitable section of a ROM with a fixed size.
    /// </summary>
    /// <remarks>
    /// For example, weapon data, or enemy data.
    /// </remarks>
    abstract class FixedWidthBlock<T> : IRomBlock<T>, IRomSerializable, IRomValueList<T> where T : IRomValueType {
        protected abstract T Build(byte[] buf);

        public FixedWidthBlock(
            int address,
            int itemSize,
            int count = 1
        ) {
            if (address < 0)
                throw new ArgumentException("Starting address must be a positive integer");

            if (count < 1 || count < 1)
                throw new ArgumentException("Item definitions must be positive non-zero");

            StartAddress = address;
            ItemSize = itemSize;
            Count = count;
        }
  
        public int Count { get; }
        public int ItemSize { get; }
        public int Size => ItemSize * Count;

        public int StartAddress { get; }
        public int EndAddress => StartAddress + Size;

        public bool Loaded { get; private set; }

        List<T> items;
        public IEnumerable<T> Items {
            get => items;
            set {
                if (value.Count() != Count)
                    throw new ArgumentException($"Expected {Count} elements, got {value.Count()}");
               
                items = new List<T>(value);
            }
        }

        public T this[int index] {
            get => items[index];
            set => items[index] = value;
        }

        public void Load(IRom rom) {
            Items =
                Rom.ReadItems(rom, StartAddress, ItemSize, Count, Build);
           
            Loaded = true;
        }

        public void Save(IRom rom)
            => Rom.WriteItems(rom, StartAddress, Items);
    }
}
