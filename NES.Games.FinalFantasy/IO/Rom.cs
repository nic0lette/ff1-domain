﻿namespace NES.Games.FinalFantasy.IO {
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    /// <summary>
    /// Generic utility functions for Rom IO.
    /// </summary>
    static class Rom {
        /// <summary>
        /// Read a sequence of byte from a rom, starting at a 
        /// specific address.
        /// </summary>
        /// <param name="rom">A Final Fantasy rom.</param>
        /// <param name="address"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static byte[] Read(IRom rom, int address, int count) {
            var buf = new byte[count];
            Array.Copy(rom.Data, address, buf, 0, count);
            return buf;
        }

        public static T[] ReadItems<T>(IRom rom, int address, int size, int count, Func<byte[], T> build) {
            var bufSize = size * count;
            var data = Rom.Read(rom, address, bufSize);
            var entries = new T[count];

            for(
                int idx = 0, offset = 0;
                offset + size <= bufSize;
                ++idx, offset += size
            ) {
                var buf = new byte[size];
                Array.Copy(data, offset, buf, 0, size);
                entries[idx] = build(buf);
            }

            return entries;
        }
        

        public static T[] ReadVariableWidthItems<T>(
            IRom rom,
            int address,
            int maxLength,
            int count,
            byte separator,
            Func<byte[], T> build
        ) {
        			var buf = new List<byte>();
			var list = new List<T>();

			int tmp;

			using (var ms = new MemoryStream(rom.Data, address, maxLength)) {
				while ((tmp = (byte)ms.ReadByte()) != -1) {
					if (tmp == separator) {
						list.Add(build(buf.ToArray()));

                        if (list.Count == count) break;
						buf = new List<byte>();
                        continue;
					}

					buf.Add((byte)tmp);
				}
			}

            return list.ToArray();
        }

        public static void Write(IRom rom, int address, byte[] data) {
            using (var ms = new MemoryStream(rom.Data)) {
                ms.Write(data, address, data.Length);
            }
        }

        public static void Write(IRom rom, int address, byte data)
            => rom.Data[address] = data;

        public static void WriteItems<T>(
            IRom rom,
            int address,
            IEnumerable<T> items
        ) where T : IRomValueType {
            var buf =
                items
                    .SelectMany(i => i.ToBytes())
                    .ToArray();

            Write(rom, address, buf);
        }
    }
}
