﻿namespace NES.Games.FinalFantasy {
    using System;
    using System.Collections.Generic;
    using System.Text;

    using NES.Games.FinalFantasy.IO;

    public enum SpellTarget : byte {
        AllEnemies = 0x01,
        OneEnemy = 0x02,
        Caster = 0x04,
        AllAllies = 0x08,
        OneAlly = 0x10
    }

    public enum SpellEffect : byte {
        None,
        Damage,
        DamageUndead,
        /// <remarks>
        /// Debuff effect depends on the spell's power.
        /// <see cref="Spell" />
        /// </remarks>
        Debuff,
        LowerHitMultiplier,
        LowerMorale,
        // UNKNOWN = 0x06,
        RaiseHP = 0x07,
        /// <remarks>
        /// Status effect depends on the spell's power.
        /// <see cref="Spell" />
        /// </remarks>
        CureStatus = 0x08,
        RaiseAbsorb = 0x09,
        SetResistance = 0x0A,
        // UNKNOWN = 0x0B,
        RaiseHitMultiplier = 0x0C,
        // Will raise hit% by spell hit and damage by spell power.
        RaiseOffense = 0x0D,
        LowerEvasion = 0x0E,
        SetHPToMax = 0x0F,
        RaiseEvasion = 0x10,
        RemoveAllResistances = 0x11,
        // If CurHP <= 300, Set status
        PowerWordDebuff = 0x12
    }

    public enum School {
        Black,
        White,
    }

    /// <summary>
    /// Represents one class' spell permissions.
    /// </summary>
    // Counterintuitively, spell permission bits are set to 1
    // to deny the ability to learn a spell, not to allow it.
    // 
    // The permissions for each level are one byte, with each bit
    // representing the spell in the corresponding slot. The low bits
    // are black magic, the high bits are white.
    // Therefore, bit 1 of the first byte would be FIRE,
    // while bit 6 of the first byte would be HARM.
    public class SpellPermission : IRomValueType {
        byte[] raw;

        public int Size => 8;

        public SpellPermission(byte[] data) {
            if (data.Length != 8)
                throw new ArgumentException("Spell permissions must be exactly 8 bytes");
            
            raw = data;
        }

        /// <summary>
        /// Returns whether a specific spell is castable or not.
        /// </summary>
        /// <param name="school">The spell's school.</param>
        /// <param name="level">The spell's level.</param>
        /// <param name="slot">The spell's slot within its level.</param>
        /// <returns></returns>
        public bool CanCast(School school, int level, int slot) {
            ValidateSpell(level, slot);

            return !((raw[level] & GetMask(school, slot)) != 0);
        }

        /// <summary>
        /// Set a specific spell as learnable.
        /// </summary>
        /// <param name="school">The spell's school.</param>
        /// <param name="level">The spell's level.</param>
        /// <param name="slot">The spell's slot within its level.</param>
        /// <returns></returns>
        public void Enable(School school, int level, int slot) {
            ValidateSpell(level, slot);

            raw[level] &= (byte)~GetMask(school, slot);
        }

        /// <summary>
        /// Prevent a specific spell from being learnable.
        /// </summary>
        /// <param name="school">The spell's school.</param>
        /// <param name="level">The spell's level.</param>
        /// <param name="slot">The spell's slot within its level.</param>
        /// <returns></returns>
        public void Disable(School school, int level, int slot) {
            ValidateSpell(level, slot);
            
            raw[level] |= GetMask(school, slot);
        }

        // Raise an exception if level and slot don't
        // match a spell.
        void ValidateSpell(int level, int slot) {
            if (slot < 1 || slot > 4)
                throw new ArgumentException("There are only 4 slots per level. Expected slot: 1-4");

            if (level < 1 || level > 8)
                throw new ArgumentException("There are only 8 spell levels. Expected level: 1-8");
        }

        byte GetMask(School school, int slot) {
            // White magic is stored in the high bits, black in the low bits.
            if (school == School.White) slot *= 2;

            return (byte)(1 << (slot - 1));
        }

        public byte[] ToBytes() => raw;
    }


    class SpellPermissionBlock : FixedWidthBlock<SpellPermission> {
        public SpellPermissionBlock(int address) : base(address, 8, 12) {}
        
        public SpellPermissionBlock(int address, IRom rom) : base(address, 8, 12) {
            Load(rom);
        }

        protected override SpellPermission Build(byte[] buf) => new SpellPermission(buf);
    }
}
